package edu.iastate.cpre388.lecture23_services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class ForegroundService extends Service {
    private static final String NOTIFICATION_CHANNEL = "foregroundService";
    private static final int NOTIFICATION_ID = 1;

    public ForegroundService() {
    }

    public static void startAction(Context context) {
        Intent intent = new Intent(context, ForegroundService.class);
        context.startService(intent);
    }

    public static void stopAction(Context context) {
        Intent intent = new Intent(context, ForegroundService.class);
        context.stopService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create a notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL);
        builder
                .setContentTitle("Foreground Service")
                .setContentText("This is the foreground service's notification")
                .setSmallIcon(R.drawable.fg_icon);
        // Promote this service to a foreground service
        startForeground(NOTIFICATION_ID, builder.build());

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
