package edu.iastate.cpre388.lecture23_services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Button mGetButton;
    private Random mRand = new Random();
    private BoundBinderService mService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGetButton = findViewById(R.id.getButton);
    }

    public void onBackgroundClick(View v) {
        BackgroundIntentService.startAction(this);
    }

    public void onBackground2Click(View v) {
        BackgroundService.startAction(this);
    }

    public void onBackgroundStopClick(View v) {
        BackgroundService.stopAction(this);
        BackgroundIntentService.stopAction(this);
    }


    public void onForegroundClick(View v) {
        ForegroundService.startAction(this);
    }

    public void onForegroundStopClick(View v) {
        ForegroundService.stopAction(this);
    }


    public void onBindClick(View v) {
        Intent intent = new Intent(this, BoundBinderService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void onUnbindClick(View v) {
        unbindService(mConnection);
        mService = null;
    }

    public void onSetClick(View v) {
        mService.setVal(mRand.nextInt());
    }

    public void onGetClick(View v) {
        int val = mService.getVal();
        mGetButton.setText(Integer.toString(val));
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BoundBinderService.LocalBinder binder = (BoundBinderService.LocalBinder) service;
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };
}
