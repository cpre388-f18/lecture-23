package edu.iastate.cpre388.lecture23_services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class BoundBinderService extends Service {
    private final IBinder mBinder = new LocalBinder();
    private int mVal = 0;

    public BoundBinderService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        BoundBinderService getService() {
            return BoundBinderService.this;
        }
    }

    public void setVal(int val) {
        mVal = val;
    }

    public int getVal() {
        return mVal;
    }
}
