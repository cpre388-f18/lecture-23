package edu.iastate.cpre388.lecture23_services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

public class BackgroundIntentService extends IntentService {
    private static final String TAG = "BackgroundIntentService";

    public BackgroundIntentService() {
        super("BackgroundIntentService");
    }

    public static void startAction(Context context) {
        Intent intent = new Intent(context, BackgroundIntentService.class);
        context.startService(intent);
    }

    public static void stopAction(Context context) {
        Intent intent = new Intent(context, BackgroundIntentService.class);
        context.stopService(intent);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Stopping");
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Doing task");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Task done");
    }
}
